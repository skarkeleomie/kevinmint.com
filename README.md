## kevinmint.com

My blog by Nuxt.js & online site: https://www.kevinmint.com

## Build Setup

```
# install dependencies
$ npm install # Or yarn

# serve with hot reload at localhost:999
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start
```

## Mobile

![image](https://qiniu.kevinmint.com/blogpc.png)

## Mobile

![image](https://qiniu.kevinmint.com/blogmobile.png)

## 目录结构

```

Project
  │
  ├── assets                # 资源目录
  |
  ├── components            # 组件目录
  |
  ├── layouts               # 布局目录
  |
  ├── pages                 # 页面目录
  |
  ├── plugins               # 插件目录
  |
  ├── static                # 静态文件目录
  |
  ├── store                 # Store目录
  |
  ├── .gitignore            # Git忽略文件配置
  |
  ├── api.config.js         # 请求地址配置文件
  |
  ├── nuxt.config.js        # Nuxt配置文件
  │
  ├── package.json          # 项目配置
  │
  └── yarn.lock             # 项目依赖版本控制

```